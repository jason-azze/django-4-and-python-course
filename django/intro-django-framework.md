Sections 44 - 45

Django created in 2003 and open sourced in 2005.

It was started at a newspaper, so had/has a culture of good documentation.

Django is a Python based framework for creating web applications.

Framework: rules, structures, functionality

Django has a lot of Python modules that take care of commone web application features.

# How Django works

- Model-Template-View MTV
    - Model connects to database
    - Template connects to user browser
    - View links te model and templated together

ORM - object relational mapper

user browser reads a template, which is an html file {{JINJA}}

jinja lets you get Python stuff into the template directly.


the models file interfaces with the database on one side and the view(s) on the other

Django is heavily reliant on the idea of a Model.
The Model is a Python/Django representation of a table in a database.
This makes it very easy to work with querying data, but does add the requirement of understanding Models and setting them up for views.


