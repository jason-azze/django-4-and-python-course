"Connect a URL route to a particular view."

We're starting with function based views, but later we will discuss class based views (CBVs). But we need to learn about models first.

Project Application Exercise
- Create a Django Project called "my_site" with an app called "first_app".

```
django-admin startproject my_site
cd my_site
python3 manage.py startapp first_app
```


