47 - First Django Application (App)

Django projects can have separated components called apps. Don't get confused by this nomenclature.

A web app usually describes a full website or mobile app.

A Django app is a sub-component of a single Django Project.

Each app should cover a different key function of your web site.

To create a new app:

```
python3 manage.py startapp app_name
```

Now we want to create a view.

We have just one app for use in the course right now. And that's pretty typical for small projects.

There is a distinction between things managed at the project level and things at the app level.

The app has a views.py file.

This file allows us to create something to be displayed in the users browser.

We _could_ directly connect our app's views.py to the project level urls.py. That architecture works for certain kinds of (small) projects.

However, for more complex projects, we create a urls.py inside the app and then connect that urls.py to the project level urls.py.

We're going to practice the second method. The app level urls.py isn't automatically created for us, so we make one.
