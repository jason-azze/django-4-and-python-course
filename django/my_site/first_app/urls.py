from django.urls import path
from . import views

# Create a list, then head over to the project-level urls.py
# Over in the project-level urls.py, we included this file, so there is now assumed
# to be a first_app/ in front of the patterns we list below.
# In exercise 53 we are linking our sports and finance pages to urls here
urlpatterns = [
    path('<int:num_page>',views.num_page_view), # Added in ex 55 redirect basics
    path('<str:topic>/',views.news_view,name="topic-page"), # Added a name in ex 56. Now we can refer to this path by name
    path('<int:num1>/<int:num2>',views.add_view) # path converter for int for our add_view view
]