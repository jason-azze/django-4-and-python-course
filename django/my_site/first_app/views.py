from unittest import result
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseNotFound, Http404, HttpResponseRedirect
from django.urls import reverse


# Around 4:00 in ex 53, let's try to be more Pythonic and use a dictionary for all of our views. At 5:00 this is more clear
# Instead of creating individual function definitions for each view, we're going to make a single function that can dynamically
# generate the views. Instead of only taking request as an argument, it will also take a topic.

articles = {
    'sports':'Sports Page',
    'finance':'Finance Page',
    'politics':'Politics Page'
}

def news_view(request,topic): # urls.py modified to match on a generic topic
    try:
        result = articles[topic]
        return HttpResponse(articles[topic])
    except:
        raise Http404("404 GENERIC ERROR") # later on we'll connect this to a custom 404.html template.
        # To see this, you need DEBUG = False and ALLOWED_HOSTS = ["127.0.0.1"] in settings.py


# at 9:00 in ex 53
def add_view(request,num1,num2):
    # The user can give num1 and num2 thusly domain.com/first_app/num1/num2 --> num1+num2
    add_result = num1 + num2
    result = f"{num1}+{num2} = {add_result}"
    return HttpResponse(str(result))

# "I need to conect these multiple views to a url."

# Adding in Exercise 55. Redirect basics
# domain.com/first_app/0 ---> domain.com/first_app/sports

def num_page_view(request,num_page):
    # Grab all the keys from dictionary articles and create a list from that
    topics_list = list(articles.keys()) # ['sports','finance','politics']
    topic = topics_list[num_page]

    return HttpResponseRedirect(reverse('topic-page',args=[topic])) # And now as you would typically see reverse used