## 55. Redirects

Sometimes we want to redirect our client.
We do this by using HttpResponseRedirect()

Let's say we wanted to redirect out website reader from "page 2" of the website to "finance" `first_app/2/` --> `first_app/finance`

The example in this exercise is going to be very simple and, in fact, not the way you'd normally do it. But we don't know how
to use templates and other features yet.
