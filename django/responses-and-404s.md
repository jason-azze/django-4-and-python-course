Ex 54 - Using ResponseNotFound and 404 Pages

How do we return a helpful 404 instead of a Django error when a user tries to hit a view that does not exist or when a client provides an incorrect URL route?

`HttpResponseNotFound()`

Django also has a default 404 page.

Now back to editing code.
