# 51. Views and URLs Overview

- Views dictate _what_ information is being show to the client.
- URLs dictate _where_ that information is shown on the website.
- You can think of each View/URL pairing as a web page on the website.

"The View can't do it alone; it needs to be routed through a URL."

- A list of view routes is defined in a list variable called urlpatterns

- You connect a View to a URL with a `path()` function call. `path()` takes two arguments.

    - route: String code that contains the URL pattern. Django will scan the relevant urlpatterns list until it finds a match (e.g. `app/`).
    - view: Once a match is found, the view argument connects to a function or view, typically defined in the `views.py` of the relevant App.

    - (Optional arguments) kwargs - keyword arguments passed as a dictionary and, name - Allows us to name a URL so we can reference it elsewhere

# 52. Function Based Views - Basics

It's going to be hard to takes notes on this part because I can't just annotate Python files in vim as we were doing before.
I've got VS Code open with the Django site skeleton.
Created a `urls.py` in the `first_app` directory. This is the App-level urls file, as opposed to the project level one.
We're going to make a simple view in the App's `views.py` and presumably link it to a url. The action will take place in those files.



