## 56 Reverse URLs and URL Names

We're going to be adding a lot of views and URLs

URL paths can be given names that we can refer to elsewhere

Django also has a reverse path that we can use.
- We can refer to URLs by name in, let's say, redirects
- There is a reverse() function, too, which can take a url name
- It's a "reverse" because normally the urls.py is asking the views.py where stuff is, but now the views.py is going to ask urls.py "what url goes with this name?"

For this exercise we're going to name a url and use a reverse.

