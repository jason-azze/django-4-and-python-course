# Control Flow: For Loops

mylist = [1,2,3,4,5]

for item in mylist:
    print(item)

# You don't need to refer to your iterable in you operation.

for item in mylist:
    print("Hello")

# Works the same for tuples

dogs = (1,2,3,4)

for furcreature in dogs:
    print(f"Doge number {furcreature}")


# What about dictionaries?

employees = {'ceo':'Dorkas','cfo':'Baby Tuckoo'}

for thingy in employees:
    print(thingy) # prints the keys
    print(employees[thingy]) # prints the values and not the key

# This is better
for position in employees:
    print(f"The {position} postion is held by {employees[position]}")

# Unpack some dictionary tuples (tuple unpacking)
print(employees.items())

hislist = [('a','b'),('c','d'),(1,2)] # A list of tuples

for item in hislist:
    print(item)

# We can do tuple unpacking
for item1,item2 in hislist:
    print(item1)

# The exercise:
# TASK: Write a for loop that fills my_list2.
my_list1 = [1, 2, 3, 4, 5, 10]
my_list2 = []
my_list3 = []

for value in my_list1:
    newvalue = (value + 42)
    my_list2.append(newvalue)

# The instructor's solution skipped the intermediate variable assignment.
for item in my_list1:
    my_list3.append(item+42)

print(my_list2)
print(my_list3)
