# 36 Errors and Exception Handling

# Try, Except, Finally

try:
    print("hello")
except:
    print("error!")

try:
    print("10" +10) # intentional type mismatch
except:
    print("error error!")

try:
    print("10" +10) # intentional type mismatch
except TypeError:   # catch a specific type of error
    print("You are using the wrong data types.")

try:
    print("France") # intentional type mismatch
except IOError: # Stack up exceptions
    print("You have an input/output error.")
except TypeError:
    print("You are using the wrong data types.")
except:
    print("Hey, you have an error.") # catchall
else:
    print("ELSE BLOCK RAN")

try:
    print("Canada") # intentional type mismatch
except IOError: # Stack up exceptions
    print("You have an input/output error.")
except TypeError:
    print("You are using the wrong data types.")
except:
    print("Hey, you have an error.") # catchall
finally:
    print("FINALLY WILL ALWAYS RUN, ERROR OR NO ERROR")

# Write a function called divider(a,b) that accepts two values and returns
# the result a/b. When b is zero Python throws and exception. Write an
# error handling routing that prints "Please do not divide by zero!" if ZeroDivisionError is raised.


def divider(a,b):
    return a/b

try:
    divider(1,22)
except ZeroDivisionError:
    print("Please do not divide by zero!")

# The above was marked incorrect even though it meets the spec.
# The instructor solution looked like:

def divider(a, b):
    try:
        return a/b

    except ZeroDivisionError:
        print("Please do not divide by zero!")
# Yes, it makes sense to have the error handling associated with the use of the function included in the function itself.
