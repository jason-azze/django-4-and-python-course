# key value pairs
employees = {"chef":"Amy","ceo":"Jason"} # a dictionary

print(employees["ceo"])

employees["waiter"] = "Mike" # Add a new key-value pair

print(employees["chef"])
print(employees["waiter"])


print(employees)

employees["waiter"] = "Ted" # Update
print(employees["waiter"])
print(employees)

print(employees["chef"].upper()) # Use a method on something we looked up.

# Question: What if I had two or more waiters?

stock_prices = {"GOOGL":[200,210,220],"GME":[20,100,300]} # A dictionary with a list to maintain an ordered history

print(stock_prices["GOOGL"])

history = stock_prices["GOOGL"]

print(f"First day price is: {history[0]}")

# Nested dictionaries

mydict = {"OUTER": {"INNER":100} } # The value of the key "OUTER" is another dict.

print(mydict["OUTER"])
print(mydict["OUTER"]["INNER"]) # I don't understand why this works. "From this dictionary, grab the INNER key-value"

# Some methods to use with dictionaries

anotherdict = {'key1':100,'key2':200,'key3':400}
print(anotherdict.keys()) # Gives back a "specialized object" called dict_keys that looks like a list
print(anotherdict.values()) # Same with values
print(anotherdict.items()) # Gives us tuples


