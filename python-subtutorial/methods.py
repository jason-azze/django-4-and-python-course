# 39 Python OOP - Methods

# They're really just functions defined inside the body of a class.
# They are used to perform operations with the attributes of our objects.

class Circle():
   
    pi = 3.14 # A class object attribute, true for every instance of the class.

    # What are the typical "attributes" of a circle? One is radius.
    def __init__(self,radius=1): # set the default value of radius to 1 in case the user doesn't pass in a value
        self.radius = radius # self dot radius is equal to the radius passed in by the user

    # The __init__ is technically a "method", but let's add some more methods.

    # Calculate the area of a circle
    def area(self): # passing in self 'gives you access' to the other attributes defined in the class.
        return self.radius*self.radius*self.pi # we could also have used Circle.pi

        
    # Calculate the perimeter
    def perimeter(self):
        return 2*self.pi*self.radius

    # Area, but with intermediate variables
    def area2(self):
        result = self.radius**2
        result = result * self.pi

        return result

    # A silly method to permanently change an attribute value
    def double_radius(self):
        self.radius = self.radius*2
        print(f'Radius has been doubled to {self.radius}')


    # Methods can take in their own parameters, too.
    def multiply_radius(self,multiplier):
        self.radius = self.radius * multiplier
        print(f'Radius has been changed to {self.radius}')

mycircle = Circle(radius=4)
print(mycircle.radius)
print(mycircle.area())
print(mycircle.perimeter())
print(mycircle.area2())
mycircle.double_radius() # You don't need to print anything because nothing is being returned.
mycircle.multiply_radius(multiplier=2)


# Coding Exercise 23: Python OOP - Methods
# Create a class Dog that takes the dog's name, breed, and age.
# Define a method called calculate_human_age that multiplies the dog's age by 7 and returns the result.
# Make sure that the calculate_human_age does not accept any arguments besides self.
# Use this class to create a 7 year old German Shepherd called Hans and find out its human age.

class Dog():

    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def calculate_human_age(self):
        return 7 * self.age

mydog = Dog(name="Hans", breed="German Shepherd", age=7)
print(f"{mydog.name} is {mydog.calculate_human_age()} in human years.")
        
