# exponents
print(2**3)

# integer
my_income = 100
# float
tax_rate = 0.1

# result of integer * float is a float
my_taxes = my_income*tax_rate
print(my_taxes)

# strings of characters are indexed starting at zero
# so the letter a here has index 0
stringy = "abcdefg"

# You can take slices of strings using square bracket notation
# To slice index 0, 1, 2 do [0:2]

# Python has built-in methods that we can call directly from the 
# string to transform it, such as making it all uppercase.

# Some method calls on strings
print("Transformations")
myvar = "hello"
print(myvar)
print(myvar.upper())
print(f"{myvar}\n".capitalize())

# split by default will split on spaces
print("Splitting")
gumple = "Hello Mike, how is your dog"
print(gumple)
print(gumple.split())
print(gumple.split(","))
print(gumple.split("o"))
print("\n")

# indexing
print("Indexing")
print(gumple[0])
print(gumple[-1]) # counting backwards indexing
print(gumple[0:5]) # slice up to but not including endpoint starting at index 0
print("\n")

# check-in exercise
input_string = "I love programming with python!"
print(input_string[2:6].upper())