# Python Functions - exercise 34
# Functions help you stay DRY
# Functions require specific syntax, including the def keyword, correct indentation, and proper structure.

# "Snake casing" is all lowercase words separated by underscores. It is preferred for function names.
# Later we will pass arguments in via parens ().
# A colon indicates an upcoming indented block. Everything indented is inside the function.
def name_of_function():
    '''
    Docstring explains the function and will be displayed
    if somebody passes in the name of your function to
    Python's built-in help.
    '''
    print("Hello")

name_of_function() # Call the function

# Add arguments
def name_of_function(name):
    '''
    Docstring explains the function and will be displayed
    if somebody passes in the name of your function to
    Python's built-in help.
    '''
    print("Hello, " +name)

name_of_function("Jason")

# But we usually want to do more than print with a function.
# The return keyword sends back the result of the function,
# allowing us to assign the function's output to a new variable.

# Our function takes in two arguments and returns a result that
# we can store in a variable.

def add_function(num1,num2):
    return num1+num2

result = add_function(1,2)
print(result)

# Using an f-string literal

def say_hello(name):
    return  f"Hello, {name}"

myvar = say_hello("Fred")
# Note the instructor explicitly set name like so: myvar = say_hello(name="Fred")
print(myvar)

# He addresses this in the lecture seconds later. You can pass in arguments "by position", but
# it is safer to be explicit. It is also kinder to readers of your code because the variable 
# names might be set hundreds of lines of code above where you are reading at the moment.

def yell_hello(first_name,last_name):
    return f"Hello, {first_name} {last_name}"

yellin = yell_hello(first_name="Fred",last_name="Gwynn")
print(yellin)
# or
shoutin = yell_hello("Fred","Gwynn")
print(shoutin)

# Check if your number is greater than or equal to 100
def checker(num):
    if num > 100:
        return "greater than 100"
    else:
        return "not greater than 100"

print(checker(num=90))
print(checker(num=900))

# Getting fancier
mylist = [1,2,5,3,5,7,10,2,4,6,2,7,3,9,4] # Imagine a datastream coming in
# I want to know if the number 10 is in our list.

def tenchecker(list_to_check):
    for num in list_to_check:
        if num == 10:
         return True # Once you "hit a return" the function ceases to operate
    return False # this return is aligned with the for block so we go through every number in the list and only reach this line if nothing matches
# If we had done an else statement inside the for block that returned false, the first number would be evaluated, not match 10, and our else would fire and the function would exit
print(tenchecker(mylist))

# Task: Implement a function called calculator that accepts a and b and a string operator which is either + or -.
# The function should return the result a operator b.

def calculator(a,b,operator):
    if operator == "+":
        return a+b
    if operator == "-":
        return a-b
    print("Please use + or -")

print(calculator(1,5,"-"))
print(calculator(1,5,"+"))

# The instructor solution uses else, which seems obvious now.

def calculator2(a, b, operator):
    if operator == "+":
        return a+b
    else:
        return a-b

# However, as I wrote in a comment on the solution oin github, this is not correct. If
# one sets operator = to hotdogs, then subtraction is performed.

print(calculator2(1,5,"hotdogs"))
