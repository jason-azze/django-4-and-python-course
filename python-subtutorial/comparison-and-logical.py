# Control Flow: Comparison and Logical Operators

print(1 > 2)
print(1 < 2)
print(1 >= 2)
print(1 <= 2)
print(1 != 2)
print(1 == 2) # comparison

record = "jose"
match = "jose"

print(record == match)

# Is one greater that two and two less than three?
print( 1>2 and 2<3 )
print( 1>2 or 2<3 ) # I tried xor, but it didn't exist

my_string1 = "Alpha"
my_string2 = "Anaconda"
print(my_string1[0] == "A" and my_string1[4] == "a" and my_string2[0] == "A" and my_string2[7] == "a")


