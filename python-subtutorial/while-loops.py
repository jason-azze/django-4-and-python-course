# While Loops - exercise 33

n = 1

while n<5:
    print(f"N is currently: {n}")
    n = n + 1

# Task: Fill my_list with values 0 to 1000 inclusive.
my_list = []
print(my_list)

n = 0

while n<=1000:
    my_list.append(n)
    n = n + 1

print(my_list)
    
