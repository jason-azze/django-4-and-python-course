# 38 - Class and Attributes

# In Python, everything is an object. How can we prove this?

num = 10
print(type(num)) # type is a built-in function

foo = 10.2
print(type(foo))

bar = '200'
print(type(bar))

"""
We get back
<class 'int'>
<class 'float'>
<class 'str'>
I don't think this proves everything is an object, Teach. I think it suggests everything might be a class.
"""

# The simplest class possible (probably).
# By convention, use camel case.
class Sample(): # the parens are optional and sometimes you won't see them in documentation or other code, but Teach likes to preserve them.
    pass # A keyword that says "do nothing"

# Now let's make an instance of our class.
num = Sample() # num is equal to an instance of sample
print(type(num))

"""
This gives us
<class '__main__.Sample'>
__main__ tells us that our instance is running in the same Python file that the class was defined in
"""

# Define attributes in a class
# We start with an "instantiation method"
class Foonly():

    def __init__(self,name): # Technically speaking it doesn't need to be self, but by convention we always use self. Wait, so then it's not a "keyword" by my definition. Allows us to refer to attributes of the class from any other method within that class.

        self.name = name # allows self.name to be used later in any method in this class. The user upon creating an instance of a Foonly class will have to provide "name".


x = Foonly(name="Bob") # the name= is optional but makes the code more readable later when class instances are far away from the instantiations.
print(type(x))

print(x) # I asked myself why not just print x. Well, you can. You get <__main__.Foonly object at 0x100673880>
print(x.name)

# More parameters and attributes
class Student():
    
    planet = "Earth" # Our Students can have different names and GPAs, but we know every instance will be on Earth. This is called a "Class Object Attribute". It's going to be the same for every instance of a Student that we create.

    def __init__(self,name,gpa): # We can think of name and gpa as "descriptors" of a Student.

        self.name = name # This is just an "Attribute"
        self.gpa = gpa

stu1 = Student(name="Jose", gpa=4.0)
sut1 = Student(name="Mimi", gpa=3.5)

print(stu1.gpa)
print(stu1.planet)

# we can update student one's gpa attribute.
stu1.gpa = 3.2
print(stu1.gpa)

# Check-In Exercise: Python OOP - Class and Attributes
# Create a class Dog which accepts the dog's name and breed.
# Use this class to create two dogs, a German Shepherd called Hans and a Labrador called Lou.
# Finally, use and f-string to print the name of your two dogs (using an attribute call like var.name)
# within the phrase "Hans and Lou are friends"

class Dog():
    
    def __init__(self,name,breed):
        
        self.name = name
        self.breed = breed

dog1 = Dog(name="Hans", breed="German Shepherd")
dog2 = Dog(name="Lou", breed="Labrador")

print(f"{dog1.name} and {dog2.name} are friends")
