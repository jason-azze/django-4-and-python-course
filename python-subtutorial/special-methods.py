# 41. Pythond OOP - Special Methods

# Sometimes called dunder methods (double-under) because they use __.

# Remeber, Python itself has pre-defined functions.

mylist = [1, 2, 3]
print(len(mylist)) # Python "knows" the length of this list. But how might it know what the length of a _user defined_ object is?

class Book():

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

mybook = Book(title = "Python Rocks!", author = "Jose", pages = 120 )
# What happens if I try to print just mybook?
print(mybook)
"""
<__main__.Book object at 0x1003ca8e0>
Python says "Inside your main script, you've created a Book object, which is at this location in memory.
This is not super useful.
And if you tried to print the length of the book assuming Python would magically know that the pages are what you mean by length, you'd just get an error.
TypeError: object of type 'Book' has no len()
"""

# How can we create out own, custom string representation of an object?
# How about a custom length?

class Book2():

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self): # Python gives us this __str__ method
        return f"{self.title} written by {self.author}" # Return a custom string representation of my book

mybook2 = Book2(title = "Python Rocks!", author = "Jose", pages = 120)
print(mybook2) # We're going to use this technique a lot with Django.

# Now let's add length

class Book3():

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return f"{self.title} written by {self.author}"

    def __len__(self): # has to return an integer
        return self.pages

mybook3 = Book3(title = "Python Rocks!", author = "Jose", pages = 120)
print(mybook3)
print(len(mybook3))


# Coding Exercise
# Create a class called Students which accepts a list of names called names.
# Implement functionality that returns the number of students your class object holds
# and another function which defines what happens if we want to print our instance.
# When printing, it should show the names of all the students.

class Students():

    def __init__(self, names):
        self.names = names

    def __len__(self):
        return self.names

    def __str__(self):
        return f"{self.names}"

history101 = Students(names = ["Bob", "Mary", "Greg", "Alice"])
print(history101)
print(len(history101.names))

# Mine worked, but this is the official solution. Note that len is used in the special method.
class Students:
    def __init__(self, names):
        self.names = names
        
    def __len__(self):
        return len(self.names)
        
    def __str__(self):
        return f"{self.names}"

students = Students(["A", "B", "C"])
print(students)
print(len(students))
