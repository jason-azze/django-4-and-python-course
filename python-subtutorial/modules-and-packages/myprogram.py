# myprogram.py wants to use functionality from mymodule.py
# These files are in the same directory, and that's important right now.
# Later we'll see how to use a package directory.

from mymodule import useful_func # We can do this because mymodule.py is in the same directory as this file.
from mymodule import UsefulClass # classes, too. We could have imported both on one line using commas.
from mypackage.mysubmodule import my_sub_func # The __init__.py file in the directory named mypackage tells Python that we can import from other files in that dir.

# Now we can use the function here.
useful_func()

# And classes
myinstance = UsefulClass(message = "France is a country")

myinstance.report()

my_sub_func()

