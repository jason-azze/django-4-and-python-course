# OOP allows programmers to create their own objects that have methods and attributes
# These methods act as functions that use information about the object to return results or change the object.
# For example, appending to a list or counting the occurences of an element in a tuple.

# Why do we need to create our own objects?
#    for large programs, functions by themselves aren't enough for organization and repeatability.
#    Django uses it a lot.

# Classes use CamelCase instead of snake_case. This helps you identify them when they are used later.

# "self" is a keyword. It assigns your parameters to a particular instance of the class.
# init is the initialization or instantiation

class NameOfClass():
    
    def __init__(self,param1,param2):
        self.param1 = param1 # param1 is what the user passes in and self.param1 is how we link it to an intance of the class
        self.param2 = param2
    
    # The method takes in the "self" keyword argument, which lets Python know that our method can use information from an instance of the class.
    # This allows it to call self.param1 in the print statement.
    def some_method(self): # methods look a lot like functions, but inside the class. "methods in the class or that act on the class"
        # perform some action
        print(self.param1)


