# Control Flow: if, elif, else
# Whitespace starts to matter

if True:
    print("True!")
    print("Another line")

password = "mypassword12312"
stored_pw = "mypassword"
admin = True

some_condition = password == stored_pw # some condition will be True or False 

if some_condition:  # is True
    print('passwords match')

# Or skip the intermediate stage of variable assignment and evaluate in the if

if password == stored_pw:
    print('passwords match')

if password == stored_pw:
    print('passwords match')
else:
    print('no password match')

if password == stored_pw:
    print('passwords match')
elif admin:
    print('passwords did not match, but admin granted') # execution falls through until it hits a match, then it stops
else:
    print('no password match and not an admin')

a = 23
b = 8

if a + b == 42:
    print(42)
elif a + b >= 30 and a + b <= 41:
    print(a + b)
else:
    print("False")

