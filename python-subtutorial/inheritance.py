# 40. Python OOP - Inheritance

# Inheritance allows us to pass in an existing class (maybe a class made by the Django devs
# in our case) to a new class, where we get to inherit the use of the methods already defined
# within the parent class, including the __init__ method.

# The parent class is called a Base Class
# The child class is called the Derived Class

class Person():

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def hello(self):
        print("hello")

    def report(self):
        print(f"I am {self.first_name} {self.last_name}.")

jello = Person(first_name="Jason", last_name="Smith")
jello.hello()
jello.report()


# Now use inheritance
class Agent(Person): # I now have access to the methods of Person.

    def reveal(self, passcode):

        if passcode == 123:
            print("I am a secret agent!")
        else:
            self.report() # Use the report method from Person

    def report(self):
        print("I am here.") # We can override the behavior of an inherited method.

x = Person(first_name="Carlos", last_name = "Bandycoot")
x.report()

y = Agent(first_name = "Carlos", last_name = "Bandycoot") # An instance of Agent has to be contructed in the same way as its base class.
y.hello() # We get access to the methods in the base class.
y.reveal(456) # This is the wrong passcode, but instead of report revealing our Agent's name, we have overridden the behavior of that method.
y.reveal(123)

class FancyAgent(Person):

    def __init__(self, first_name, last_name, code_name): # We can extend the attributes of a base class. Not done much in Django, but Teach wants us to know.
        Person.__init__(self, first_name, last_name) # Keep the base class' attributes
        self.code_name = code_name                   # Tack on another attribute

z = FancyAgent(first_name = "John", last_name = "Smythe", code_name = "Marmaduke")
print(z.first_name)
print(z.code_name)

# Coding Exercise: Inheritance
class Animal():
    
    def __init__(self, name, species):
        self.name = name
        self.species = species

    def greet(self):
        print(f"Hi! My name is {self.name} and I am a {self.species}")

class Cat(Animal):

    def sound(self):
        print("meow")

class Dog(Animal):
    
    def sound(self):
        print("ruff")

doggo = Dog(name = "Spike", species = "Pug")
doggo.sound()

# So, my solution above exactly meets the requirements given in the exercise, but they wanted me to extend the base class.
# This was not actually requested, and I think they miss the point of inheritance by essentially having to write as much code
# as the original class had. This is their solution.

class Animal:
    def __init__(self, name, species):
        self.name = name
        self.species = species
    def greet(self):
        print(f"Hi! My name is {self.name} and I am a {self.species}")

class Dog(Animal):
    def __init__(self, name):
        Animal.__init__(self, name, "Dog")
    def sound(self):
        print("Wuff")

class Cat(Animal):
    def __init__(self, name):
        Animal.__init__(self, name, "Cat")
    def sound(self):
        print("Meow")
