# Tuples are like immutable lists. Used we we don't expect or want to change
# the sequence of items.
# We won't often define tuples ourselves, but many functions return tuples.
# For example, the .items() method we saw in the dictionaries exercise.

mylist = [1,2,3]
mytuple = (1,2,3)

print(mytuple)

# Grab an item in our list and reassign it.
mylist[0] = "NEW"
print(mylist)
# You can't do that with the tuple. But you can still use indexes.
print(mytuple[2])

# Booleans
a = True # case matters
b = False
print(a)
print(b)

# These comparisons return booleans.
print(1>2)
print(1<2)

