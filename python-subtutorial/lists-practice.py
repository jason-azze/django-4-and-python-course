# Lists are Data Structures

myvar = 10
mylist = [1,2,3,4,myvar]
poopvar = "doodoo"

print("The original list")
print(mylist)
print("\n")

print("Index position 0")
print(mylist[0]) # index
print("\n")

print("Slice 0 to 3 non-inclusive")
print(mylist[0:3]) # slice
print("\n")

print("Append the value of poopvar")
mylist.append(poopvar) # append method
print(mylist)
print("\n")

print("Insert the value of poopvar at index 0")
mylist.insert(0,poopvar) # insert method: where, what
print(mylist)
print("\n")

print("Remove the last item using pop default and show what we removed")
item_removed = mylist.pop() # no arg defaults to last item being removed
print(mylist)
print(item_removed)
print("\n")

print("Remove the first item of the list by using pop with index")
mylist.pop(0) # you don't need to store the result in a variable
print(mylist)
print("\n")

print("Reverse the order of the list with the reverse method")
mylist.reverse()
print(mylist)
print("\n")

print("Sort a new, unsorted list")
newlist = [3,5,6,9,1,4,3,1,8,4,88,3,54]
print(newlist)
newlist.sort()
print(newlist)
print("\n")
